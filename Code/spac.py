import numpy as np
import tqdm


def saturation_vapor_pressure(ta):
    return 611 * np.exp((17.3 * ta) / (ta + 237.3))


class Spac(object):
    def __init__(self, atm, soils, species, dt, time_stamps_field='Date'):
        self.i = 0


        self.dt = dt  # time step
        # Constants
        self.ca = 1005  # heat capacity of air
        self.grav = 9.8
        self.lambda_v = 2260000
        self.Ra = 287.05
        self.Rbar = 8.31446  # Universal gas constant in J K - 1 mol - 1
        self.rho_w = 1000
        self.vonkarman = 0.41  #
        self.Vw = 18e-6  # partial molal volume of water m3 mol - 1
        self.stefboltz = 5.67040040e-8  # stefan-Boltzmann constant in Wm-2K-4

        # Dictionaries
        self.atm = atm
        self.species = species
        self.soils = soils
        self.time_stamps = self.atm[time_stamps_field]

        # precalcs aerodynamic resistance and other quantities
        self.ra = self.aerodyn_resist()
        self.gamma = self.psychrometric_const()
        self.rho_a = self.air_density()

        # states
        self.S0 = self.S(self.soils['theta0'])
        self.theta = []
        self.swp = []
        self.lwp = []
        self.Ts = []

        self.Transp = []
        self.stom_conduct = []

        # self.CHI =  #empirical paramater > 0, dimensionless
        # self.alpha =  # guard cell advantage, dimensionless
        # self.sigma =  #  fraction of the total gas diffusion resistance between the evaporating site and the guard cell
        # self.Rgprime =  # thermal resistance between the evaporating site and the guard cell
        # self.Ds = 10  # leaf to boundary layer water mole fraction gradient, mmol H2O / mol of air
        #
        # self.Theta = self.CHI * self.sigma * self.Rd / self.Vl
        # self.delta_w

    def S(self, theta):
        S = max((theta - self.soils['thetar']) / (self.soils['porosity'] - self.soils['thetar']), self.soils['thetar'])
        return S
    def aerodyn_resist(self) -> np.array:
        z = self.soils['rr'] + 2.
        zo = self.soils['rr'] * 0.1
        zd = zo * 0.7
        return (
                (np.log((z - zd) / zo)) ** 2
                /
                (self.vonkarman ** 2 * self.atm['windspeed'])
        )

    def soil_water_balance(self, theta, LET):
        porosity = self.soils['porosity']
        thetar = self.soils['thetar']
        rootdepth = self.soils['rootdepth']
        E = -LET / (self.rho_w * self.lambda_v)
        if E < 0:
            E = 0
        S = self.S(theta)
        S_old = self.S0
        if self.i > 0:
            S_old = self.S(self.theta[self.i])
        return (S - S_old) * (porosity - thetar) * rootdepth / self.dt + E

    def brooks_corey(self, theta, swp):
        psiae = self.soils['psiae']
        bclambda = self.soils['bclambda']
        S = self.S(theta)
        
        res = psiae * 0.0098 / S ** bclambda - swp
        
        return res

    def air_density(self):
        return self.atm['P'] / (self.Ra * (self.atm['air_temp'] + 273.2))

    def psychrometric_const(self):
        Pz = self.atm['P'] * ((293 - 0.0065 * 1071) / 293) ** 5.26
        return self.ca * Pz / (self.lambda_v * 0.622)

    def canopy_energy_balance(self, Ts, lwp, rs):
        H = self.sensible_heat(Ts)
        NR = self.net_radiation(Ts)
        LET = self.latent_heat(Ts, lwp, rs)

        return NR + H + LET

    def water_potential_balance(self, theta, lwp, swp, LET):
        keff = self.soils['ksat']
        bclambda = self.soils['bclambda']
        rootdepth = self.soils['rootdepth']
        rai = self.species['RAI']
        S = self.S(theta)

        # gsr in micrometers/s/MPa (1e6 to convert m to micrometer and 1e6 again to go from Pa to MPa
        gsr = ((keff * S ** (2 * bclambda + 3) * np.sqrt(rai) * 1e12) /
               (np.pi * rootdepth * self.grav * self.rho_w))

        sperry_ks = self.species['sperry_k']
        c = self.species['sperry_c']
        d = self.species['sperry_d']
        lai = self.species['LAI']

        
        gp = sperry_ks * np.exp(-(np.abs(lwp) / d) ** c)
        
            
        E = -LET / (self.rho_w * self.lambda_v)
        if E < 0:
            E = 0

        gsrp = gsr * gp * lai / (gsr + gp * lai)

        return gsrp * (lwp - swp) - E * 1e6  # m/s to micrometer/s

    def net_radiation(self, Ts):
        sw_rad = self.atm['sw_rad'][self.i]
        lw_rad = self.atm['lw_rad'][self.i]
        albedo = self.soils['albedo']
        kbeers = self.species['kbeers']
        lai = self.species['LAI']
        epsilon = self.species['emissivity']

        return ((1 - albedo) * sw_rad * (1 - np.exp(-kbeers * lai)) +
                epsilon * lw_rad -
                epsilon * self.stefboltz * (Ts + 273.15) ** 4)

    def sensible_heat(self, Ts):
        ra = self.ra[self.i]

        return 1 / ra * self.rho_a[self.i] * self.ca * (self.atm['air_temp'][self.i] - Ts)

    def latent_heat(self, Ts, lwp, rs):
        lwp = np.array(lwp)
        rh_leaf = 1  # (lwp * self.Vw / (self.Rbar * (Ts + 273.15))).clip(0, 1)
        ra = self.ra[self.i]

        rh = self.atm['rel_hum'][self.i]
        ea = saturation_vapor_pressure(self.atm['air_temp'][self.i]) * rh
        es = saturation_vapor_pressure(Ts)

        lh = 1 / (self.gamma[self.i] * (ra + rs)) * self.rho_a[self.i] * self.ca * (ea - es)
        return lh

    def solve_spac(self, theta, swp, lwp, Ts):
        def spac(x):
            
            theta, swp, lwp, Ts = x
            rs = 1 / self.gs(lwp)
            LET = self.latent_heat(Ts, lwp, rs)

            F1 = self.soil_water_balance(theta, LET)
            F2 = self.brooks_corey(theta, swp)
            F3 = self.water_potential_balance(theta, lwp, swp, LET)
            F4 = self.canopy_energy_balance(Ts, lwp, rs)

            return (F1, F2, F3, F4)

        from scipy.optimize import fsolve
        x0 = (theta, swp, lwp, Ts)
        sol = fsolve(spac, x0, full_output=True)
        if sol[2]!=1:
            print(sol[3])
        return sol

    def run_spac(self, x0, num_time_steps=None):

        theta, lwp, Ts = x0
        self.S0 = self.S(theta)
        swp = self.soils['psiae'] * 0.0098 / self.S0 ** self.soils['bclambda']
        self.theta.append(theta)
        self.swp.append(swp)
        self.lwp.append(lwp)
        self.Ts.append(Ts)

        if num_time_steps is None:
            num_time_steps = self.atm['air_temp'].size

        with tqdm.tqdm(total=num_time_steps) as pbar:
            for i in range(num_time_steps):
                sol = self.solve_spac(theta, swp, lwp, Ts)
                theta, swp, lwp, Ts = sol[0]
                self.theta.append(theta)
                self.swp.append(swp)
                self.lwp.append(lwp)
                self.Ts.append(Ts)
                rs = 1 / self.gs(lwp)
                LET = self.latent_heat(Ts, lwp, rs)
                E = -LET / (self.rho_w * self.lambda_v)
                if E < 0:
                    E = 0
                self.Transp.append(E*1000*3600)  # to mm
                self.stom_conduct.append(self.gs(lwp))

                self.i += 1
                pbar.update()

    def gs(self, lwp):
        lwp = np.asarray(lwp)
        gsmax = self.species['gsmax']
        gsmin = self.species['gsmin']
        lai = self.species['LAI']
        gsmin *= lai
        opt_temp = self.species['opt_temp']
        max_temp = self.species['max_temp']
        min_temp = self.species['min_temp']

        sw_rad = self.atm['sw_rad'][self.i]
        Ta = self.atm['air_temp'][self.i]
        rh = self.atm['rel_hum'][self.i]

        f_light_coeff = self.species['gs_light_coeff']
        f_lwp_low = self.species['lwp_min']
        f_lwp_high = self.species['lwp_max']
        f_vpd_coeff = self.species['gs_vpd_coeff']

        es = saturation_vapor_pressure(Ta)
        ea = es * rh
        vpd = es - ea

        f_light = sw_rad / (f_light_coeff + sw_rad)

        np.seterr(all='raise')
        try:
            f_temp = (((Ta - min_temp) / (opt_temp - min_temp)) * ((max_temp - Ta) /
                                                               (max_temp - opt_temp))
                  ** ((max_temp - opt_temp) / (opt_temp - min_temp))).clip(0, 1)
        except Exception as e:
            f_temp = 0


        f_vpd = np.exp(-f_vpd_coeff * vpd)
        f_psi = ((f_lwp_low - lwp) / (f_lwp_low - f_lwp_high)).clip(0, 1)

        gs = gsmax * lai * f_light * f_temp * f_vpd * f_psi
        if gs < 0.00001:
            gs = 0.00001

        return gs

    """
    			F(0) = (x(0) - Sold) * (poros - thetar) * rootdepth / dt + E;
			F(1) = psiae / powl(x(0), bclambda) - x(1);
			F(2) = NetRadCanopy(atm, x(2), emissivity, albedo, BeerK, LAI, r, c)
					+ LE + H + LET;
	"""

    # def gs(self, psi_s, pi_e, Ts, R, P):
    #
    #     wes = 611 * np.exp((17.3 * Ts) / (Ts + 237.3)) # saturation vapor pressure at leaf tempearture
    #     delta_w = self.Ds / P
    #
    #     num = self.CHI * self.alpha * (psi_s + pi_e) - self.Theta * delta_w /wes
    #     den = 1 + self.CHI * (self.alpha * R + self.Rgprime) * delta_w
    #     return num/den


class StorageSpac(Spac):
    def __init__(self, atm, soils, species, species_stor, dt, time_stamps_field='Date'):
        super().__init__(atm, soils, species, dt, time_stamps_field)
        self.species.update(species_stor)
        self.twp0 = self.species["pressure_loss_turgor"]
        self.owp = []
        self.twp = []  # turgor pressure (turgor water potential)
        self.delta_f = []
        self.canopy_rel_wat_cont = []

    def rwc(self, p):
        """
        :param p: turgor pressure, MPa
        :return: RWC, dimensionless
        """
        rwc_lt = self.species["rwc_loss_turgor"]
        p_lt = self.species["pressure_loss_turgor"]
        k = self.species["leaf_bulk_modulus"]
        if p > p_lt:
            return 1
        return rwc_lt * np.exp((p - p_lt)/k)

    def leaf_capacitance(self, p):
        """
        :param p: turgor pressure, MPa
        :return: capacitance, MPa-1
        """
        rwc_lt = self.species["rwc_loss_turgor"]
        p_lt = self.species["pressure_loss_turgor"]
        k = self.species["leaf_bulk_modulus"]
        if p > p_lt:
            return 0

        return rwc_lt/k * self.rwc(p)

    def osmotic_potential(self, owp):

        # Gao et al 2002 eq 6
        # alpha = 2.807 kPa micromol-1 m2 s for pines (Table 3)
        # pi0 = -1795.2 kPa for pines (Table 3)

        # 1 Wm-2 = 4.57 umolm-2s-1 but PAR is ~43% of total solar radiation hence 1.96 = 0.43*4.57
        I = 1.96 * self.atm['sw_rad'][self.i]  # I is PAR in umol/m2/s

        return owp - self.species["osm_pot_dark"] + self.species["osm_sens_par"] * I

    def gs(self, p):
        ## elastic modulus of gard cell is 1/0.1189 = 8.41 kPa mmol-1 m2 s as per Gao 2002
        ## or 0.1189 mmol m-2 s-1 kPa-1
        ## since 1kgH20 = 55.55 moles:
        ## 0.0001189 mol m-2 s-1 kPa-1 / 55.55 mol/Kg = 0.00000214041 mm s-1 kPa-1
        ##  0.00000214041 mm s-1 kPa-1 * 1000 kPa/MPa = 0.00000214041 m s-1 MPa-1

        return (1/self.species["elastic_modulus_guard_cell"] * p).clip(self.species['gsmin'], np.inf) * self.species["LAI"]

    def canopy_water_balance(self, lwp, owp, delta_f):

        # TODO: having lwp be positive truly sucks, fix fix
        p = -owp - lwp

        C = self.leaf_capacitance(p)

        p_old = self.twp0
        if self.i > 0:
            p_old = self.twp[self.i - 1]

        res = C * (p - p_old)/self.dt - delta_f

        return res

    def solve_spac(self, theta, swp, lwp, Ts, owp, delta_f):
        def spac(x):
            theta, swp, lwp, Ts, owp, delta_f = x
            p = -(owp + lwp)
            rs = 1 / self.gs(p)
            LET = self.latent_heat(Ts, lwp, rs)

            F1 = self.soil_water_balance(theta, LET)
            F2 = self.brooks_corey(theta, swp)
            F3 = self.water_potential_balance(theta, lwp, swp, LET)
            F4 = self.canopy_energy_balance(Ts, lwp, rs)
            F5 = self.osmotic_potential(owp)
            F6 = self.canopy_water_balance(lwp, owp, delta_f)

            return F1, F2, F3, F4, F5, F6

        from scipy.optimize import fsolve
        x0 = (theta, swp, lwp, Ts, owp, delta_f)
        sol = fsolve(spac, x0, full_output=True)
        return sol

    def run_spac(self, x0, num_time_steps=None):

        theta, lwp, Ts, owp, delta_f = x0
        self.S0 = self.S(theta)
        swp = self.soils['psiae'] * 0.0098 / self.S0 ** self.soils['bclambda']
        self.theta.append(theta)
        self.swp.append(swp)
        self.lwp.append(lwp)
        self.Ts.append(Ts)
        self.owp.append(owp)
        self.delta_f.append(delta_f)

        if num_time_steps is None:
            num_time_steps = self.atm['air_temp'].size

        with tqdm.tqdm(total=num_time_steps, disable=False) as pbar:
            for i in range(num_time_steps):
                sol = self.solve_spac(theta, swp, lwp, Ts, owp, delta_f)
                theta, swp, lwp, Ts, owp, delta_f = sol[0]
                self.theta.append(theta)
                self.swp.append(swp)
                self.lwp.append(lwp)
                self.Ts.append(Ts)
                self.owp.append(owp)
                p = -(owp + lwp)
                self.delta_f.append(delta_f)
                rs = 1 / self.gs(p)
                LET = self.latent_heat(Ts, lwp, rs)
                E = -LET / (self.rho_w * self.lambda_v)
                if E < 0:
                    E = 0
                self.Transp.append(E * 1000 * 3600)  # to mm/h
                self.twp.append(p)
                self.stom_conduct.append(self.gs(p))
                self.canopy_rel_wat_cont.append(self.rwc(p))

                self.i += 1
                pbar.update()

