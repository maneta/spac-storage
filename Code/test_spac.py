from unittest import TestCase
import nose
import spac
import numpy as np
import pandas as pd

class TestSpac(TestCase):

    def setUp(self):
        self.atm = {
            "time_stamps": np.array(["05/01/2011 14:00:00", "05/01/2011 15:00:00"]),
            "sw_rad": np.array([300, 500]),
            "lw_rad": np.array([250, 270]),
            "P": np.array([101325, 101325]),
            "air_temp": np.array([20, 15]),
            "windspeed": np.array([2.5, 3.2]),
            "rel_hum": np.array([0.7, 0.8]),
        }
        self.specs = {
            "gsmax": 0.024,
            "gsmin": 0.00002,
            "LAI": 0.5,
            "RAI": 4,
            "opt_temp": 15,
            "max_temp": 45,
            "min_temp": 0,
            "gs_light_coeff": 350,
            "gs_vpd_coeff": 0.00001,
            "lwp_min": 7,
            "lwp_max": 0.00001,
            "kbeers": 0.55,
            "emissivity": 0.95,
            "sperry_k": 0.316,
            "sperry_d": 0.49,
            "sperry_c": 7.4,
        }

        self.soils = {
            "porosity": 0.6,
            "ksat": 0.00008,
            "thetar": 0.05,
            "rootdepth": 1,
            "psiae": 0.8,
            "bclambda": 3,
            "rr": 0.2,
            "albedo": 0.8,
            "theta0": 0.3,
        }

        self.dt = 3600

        self.Spac = spac.Spac(self.atm, self.soils, self.specs, self.dt)

    def test_brooks_corey(self) -> None:
        theta = 0.4
        S = self.S(theta)
        swp = self.soils['psiae'] * 0.0098 / S**self.soils['bclambda'];
        sol = self.spac.brooks_corey(theta, swp)
        assert (sol - 0) < 1e-12

    def test_airdensity(self) -> np.array:
        assert isinstance(self.Spac.air_density(), np.ndarray)

    def test_lat_heat(self):
        Ts = 25
        lwp = 0.75
        rs = 1 / self.Spac.gs(lwp)
        LE_comp = self.Spac.latent_heat(Ts, lwp, rs)
        LE_ref = -47.524462787215974
        assert LE_comp == LE_ref

    def test_sens_heat(self):
        Ts = 25
        LH_comp = self.Spac.sensible_heat(Ts)
        LH_ref = -0.9328422485735715
        assert LH_comp == LH_ref

    def test_net_radiation(self):
        Ts = 25
        NR_comp = self.Spac.net_radiation(Ts)
        NR_ref = -173.74780014251567
        assert NR_comp == NR_ref

    def test_solve_canopy_energy_balance(self):
        Ts = 25
        lwp = 0.75
        rs = 1 / self.Spac.gs(lwp)
        from scipy.optimize import fsolve
        ret = fsolve(self.Spac.canopy_energy_balance, Ts, args=(lwp, rs), full_output=True)
        np.testing.assert_array_almost_equal(ret[1]['fvec'], 0)

    def test_water_potential_balance(self):
        theta = 0.4
        S = self.spac.S(theta)
        swp = self.soils['psiae'] * 0.0098 / S ** self.soils['bclambda'];
        Ts = 25
        lwp = 0.75
        rs = 1 / self.Spac.gs(lwp)
        LET = self.Spac.latent_heat(Ts, lwp, rs)
        sol = self.Spac.water_potential_balance(theta, lwp, swp, LET)
        assert (sol - 0) < 1e-12

    def test_aerodyn_resist(self):
        print(self.Spac.aerodyn_resist())
        test_values = np.array([65.54  , 40.9625])
        np.testing.assert_array_almost_equal(
            self.Spac.aerodyn_resist(), test_values, decimal=4)

    def test_gs(self):
        assert (0.004996757560787419 == self.Spac.gs(-1.5))

    def test_solve_spac(self):
        theta = 0.4
        S = self.spac.S(theta)
        swp = self.soils['psiae'] * 0.0098 / S ** self.soils['bclambda']
        lwp = 0.75
        Ts = 25

        sol = self.Spac.solve_spac(theta, swp, lwp, Ts)
        print(sol)
        assert sol[2] == 1

    def test_run_spac(self):
        theta = 0.4
        S = self.spac.S(theta)
        swp = self.soils['psiae'] * 0.0098 / S ** self.soils['bclambda']
        lwp = 0.75
        Ts = 25
        x0 = (theta, lwp, Ts)
        self.Spac.run_spac(x0=x0)

    def test_run_spac_long(self):
        pd_inputs = pd.read_csv('../inputs.csv')
        atm = {
            "sw_rad": pd_inputs['Average Rad flux dens (W/m2)'].values,
            "lw_rad": pd_inputs['Ldown'].values,
            "P": np.ones_like(pd_inputs['Ldown'].values) * 101315,
            "air_temp": pd_inputs['Air Temp Marco (C)'].values,
            "windspeed": pd_inputs['WS'].values,
            "rel_hum": pd_inputs['RH'].values,
        }
        theta = 0.4
        lwp = 0.75
        Ts = 25
        x0 = (theta, lwp, Ts)
        spac2 = spac.Spac(atm, self.soils, self.specs, self.dt)
        spac2.run_spac(x0)
        print(spac2)


class TestStorageSpac(TestCase):

    def setUp(self):
        self.atm = {
            "time_stamps": np.array(["05/01/2011 14:00:00", "05/01/2011 15:00:00"]),
            "sw_rad": np.array([300, 500]),
            "lw_rad": np.array([250, 270]),
            "P": np.array([101325, 101325]),
            "air_temp": np.array([20, 15]),
            "windspeed": np.array([2.5, 3.2]),
            "rel_hum": np.array([0.7, 0.8]),
        }
        self.specs = {
            "gsmax": 0.024,
            "gsmin": 0.00002,
            "LAI": 0.5,
            "RAI": 4,
            "opt_temp": 15,
            "max_temp": 45,
            "min_temp": 0,
            "gs_light_coeff": 350,
            "gs_vpd_coeff": 0.00001,
            "lwp_min": 7,
            "lwp_max": 0.00001,
            "kbeers": 0.55,
            "emissivity": 0.95,
            "sperry_k": 0.316,
            "sperry_d": 0.49,
            "sperry_c": 7.4,
        }

        self.species_stor = {
            "osm_sens_par": 0.002807,  # MPa umol-1 m2 s of PAR
            "osm_pot_dark": -1.7952,  # MPa
            "rwc_loss_turgor": 1,
            "pressure_loss_turgor": 0.5,  # MPa
            "leaf_bulk_modulus": 1.5,  # MPa
            "elastic_modulus_guard_cell": 467200.209306  # MPa m-1 s
        }

        self.soils = {
            "porosity": 0.6,
            "ksat": 0.00008,
            "thetar": 0.05,
            "rootdepth": 1,
            "psiae": 0.8,
            "bclambda": 3,
            "rr": 0.2,
            "albedo": 0.8,
            "theta0": 0.3,
        }

        self.dt = 3600

        self.StorageSpac = spac.StorageSpac(self.atm, self.soils, self.specs, self.species_stor, self.dt)

    def test_osmotic_potential(self):
        assert (self.StorageSpac.osmotic_potential(-0.1446839999999998) < 1e10)

    def test_run_spac(self):
        theta = 0.4
        S = self.StorageSpac.S(theta)
        swp = self.soils['psiae'] * 0.0098 / S ** self.soils['bclambda']
        lwp = 0.75
        Ts = 25
        owp = -1.7952
        deltaf = 0
        x0 = (theta, lwp, Ts, owp, deltaf)
        self.StorageSpac.run_spac(x0=x0)

    def test_deb_soil_moist(self):
        pd_inputs = pd.read_csv('../inputs.csv')
        atm = {
            "time_stamps": pd_inputs['Date'],
            "sw_rad": pd_inputs['Average Rad flux dens (W/m2)'].values,
            "lw_rad": pd_inputs['Ldown'].values,
            "P": np.ones_like(pd_inputs['Ldown'].values) * 101315,
            "air_temp": pd_inputs['Air Temp Marco (C)'].values,
            "windspeed": pd_inputs['WS'].values,
            "rel_hum": pd_inputs['RH'].values,
        }

        specs = {
            "gsmax": 0.024,
            "gsmin": 0.00002,
            "LAI": 0.5,
            "RAI": 4,
            "opt_temp": 15,
            "max_temp": 45,
            "min_temp": 0,
            "gs_light_coeff": 350,
            "gs_vpd_coeff": 0.00001,
            "lwp_min": 7,
            "lwp_max": 0.00001,
            "kbeers": 0.55,
            "emissivity": 0.95,
            "sperry_k": 0.316,
            "sperry_d": 0.49,
            "sperry_c": 7.4,

        }

        soils = {
            "porosity": 0.6,
            "ksat": 0.00008,
            "thetar": 0.05,
            "rootdepth": 1,
            "psiae": 0.8,
            "bclambda": 3,
            "rr": 0.2,
            "albedo": 0.8,
            "theta0": 0.3,
        }

        species_stor = {
            "osm_sens_par": 0.002807,  # MPa umol-1 m2 s of PAR
            "osm_pot_dark": -1.7952,  # MPa
            "rwc_loss_turgor": 1,
            "pressure_loss_turgor": 0.5,  # MPa
            "leaf_bulk_modulus": 1.5,  # MPa
            "elastic_modulus_guard_cell": 267.2209306  # MPa m-1 s
        }

        model2 = spac.StorageSpac(atm, soils, specs, species_stor, 3600)
        x0 = (0.4, 0.1, 25, -1.7952, 0.001)

        model2.run_spac(x0, num_time_steps=10)





